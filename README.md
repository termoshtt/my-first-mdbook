my-first-mdbook
===============

mdbookのボイラープレートです

1. GitLabでフォークする
  - このリポジトリと関係付けたく無い場合は、ローカルにcloneして、GitLabに新たにリポジトリを作成して、pushしてください。
  - ただしGitLab CIを用いて動作しているのでGitLabでしか動きません
2. src/以下にあるMarkdownファイルに追記する
3. GitLabにpushする

でHTTPSで生成されたbook形式のHTMLがホストされます。

- URLは`https://{username}.gitlab.com/{repository_name}`になるはずです。
  - 実際には左側のSettings > Pagesのタブを見れば正しいリンクがあるはずです
- 初回のデプロイではSSLの証明書の有効化に30分程度(?)かかるはずなので気をつけてください。


手元で生成結果を確認する
-----------------------
mdbookをインストールする必要があります

### バイナリをダウンロードする

GitHubのmdbookのリポジトリのリリースのページでバイナリが配布されているのでそれを使います

https://github.com/rust-lang-nursery/mdBook/releases

tar/zipを解凍すると実行ファイルの`mdbook`が素ではいっているはずです

### cargoでインストールする

Rustの開発環境を整える場合は

```
cargo install mdbook
```

で入ります。ただコンパイルはちょっと重めです。
